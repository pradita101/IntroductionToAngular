# Welcome to Symson

## Organization, Products, Invoices, Customers, Order Lines and Competitors.
Symson is a tool that optimizes prices for an assortment of products.
For example, your local supermaket could be using Symson. 
In Symson terminology your local supermarket will be the "Organization".
  The "Products" are all the products sold by the supermarket, everything that is on the shelves. 
  The "Customers" are all the people or other companies that buy the "Products". You are one of the customers. 
  Next time you go to the supermarket, keep the receipt. In Symson terms that is an "Invoice". 
It might be helpful to keep the receipt next to you when solving these exercises. It could give a bit of extra context, a bit of extra meaning to what we're doing. 
  What's on the "Invoice"? There is a "Customer" (you), there is a date of purchase, a location and a list of products with their prices and quantities. 
This list of items on the "Invoice" are the "Order Lines".

  
These exercises are about visualizing the sales of one "Product". We have chosen the product (it's a given) and you see the list of "Order Lines" joined with the "Customer" attributes. 
We call this "Extended Order Lines". We will be visualising the list of "Order Lines" as a graph and as a table. We will then make some improvements. 

  
"Competitor" is another important concept for Symson. In our example, competitors are all the other supermakets, all the online shops and so on. 
We will not be dealing with competitors in these exercises. 

## Symson's UI team
Symson's UI team has three major roles: 
* Component specialist
* Data specialist
* Tester

This repository contains exercises for these three roles. Completing all of them would be around a day's worth of work. 
  Please implement the exercises that suit your desired role. You don't need to solve everything. Working for about two hours should be sufficient for the interview. 
  Symson uses extensively Angular Material and Highcharts. These two libraries cover most of the UI tools we develop. 

## How to solve the exercises?
Create a branch with your name and the current date. For instance, if my name is "John Smith" and today is August 2022, then please name the branch "JohnSmith_Aug2022".
  Choose your role and modify the corresponding component. Keep the Starting Code component unchanged. 
This will help with the contrast between the starting point and the result. 
  Write your solution and push. It is preferable to create an individual commit for each of the solved exercises.

  https://dev.azure.com/symson/Introduction%20To%20Symson/_workitems/recentlyupdated

  Focus on code clarity. Add comments, follow the general coding standards. 
  You can write here any extra details you might wish to add. 