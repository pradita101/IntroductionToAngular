import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTesterComponent } from './page-tester.component';

describe('PageTesterComponent', () => {
  let component: PageTesterComponent;
  let fixture: ComponentFixture<PageTesterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageTesterComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PageTesterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
