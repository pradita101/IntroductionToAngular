import { Component, OnInit } from '@angular/core';
import { ExtendedOrderLine } from '../core/models/extended-order-line.model';
import { GlobalDataService } from '../core/services/api.mock.service';

@Component({
  selector: 'app-page-tester',
  templateUrl: './page-tester.component.html',
  styleUrls: ['./page-tester.component.css']
})
export class PageTesterComponent implements OnInit {
  extendedOrderLines: Array<ExtendedOrderLine> = [];
  extendedOrderLine!: ExtendedOrderLine;
  price!: number;
  case: number = 1;
  option: Array<string> = ['price', 'date'];

  constructor(dataService: GlobalDataService) {
    this.extendedOrderLines = dataService.getExtendedOrderLines();
  }

  ngOnInit(): void {
    if (this.case === 1) {
      this.sortByOption(this.option[0]);
      this.findHighgestPrice();
    }else{
      this.sortByOption(this.option[1]);
      this.findEarliestExtendedOrderLine();
    }
  }

  sortByOption(option: string): void {
    if (option === 'date') {
      // @ts-ignore
      this.extendedOrderLines.sort((a,b) => {
        return Date.parse(a.createdDate) > Date.parse(b.createdDate);
      });
    } else {
      // @ts-ignore
      this.extendedOrderLines.sort((a,b) => {
        return a.pricePerUnit > b.pricePerUnit;
      });
    }
  }

  findHighgestPrice(): void {
    this.price = this.extendedOrderLines[0].pricePerUnit;
  }

  findLowestPrice(): void {
    this.price = this.extendedOrderLines[this.extendedOrderLines.length].pricePerUnit;
  }

  findEarliestExtendedOrderLine(): void {
    this.extendedOrderLine = this.extendedOrderLines[0];
  }

  findLatestExtendedOrderLine(): void {
      this.extendedOrderLine = this.extendedOrderLines[this.extendedOrderLines.length];
  }
}
