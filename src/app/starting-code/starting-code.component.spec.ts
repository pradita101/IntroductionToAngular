import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StartingCodeComponent } from './starting-code.component';

describe('StartingCodeComponent', () => {
  let component: StartingCodeComponent;
  let fixture: ComponentFixture<StartingCodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StartingCodeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StartingCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
