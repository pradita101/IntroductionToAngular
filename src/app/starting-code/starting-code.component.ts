import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { ExtendedOrderLine } from '../core/models/extended-order-line.model';
import { GlobalDataService } from '../core/services/api.mock.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from "@angular/material/paginator";
import {LiveAnnouncer} from '@angular/cdk/a11y';
import {MatSort, Sort} from '@angular/material/sort';

@Component({
  selector: 'app-starting-code',
  templateUrl: './starting-code.component.html',
  styleUrls: ['./starting-code.component.css']
})
export class StartingCodeComponent implements OnInit {
  extendedOrderLines: Array<ExtendedOrderLine> = [];
  tableDataSource: MatTableDataSource<any>;
  chartSeries: Array<any> = [{
    type: 'column',
    name: 'Price Per Unit',
    data: [],
  }];

  displayedColumns: string[] = [
    'createdDate', 'quantity', 'pricePerUnit',
    'customerAttributeA', 'customerAttributeB'
  ];

  constructor(
    private dataService: GlobalDataService,
    private _liveAnnouncer: LiveAnnouncer
    ) {
    this.extendedOrderLines = dataService.getExtendedOrderLines();
    console.log(typeof this.extendedOrderLines)
    this.tableDataSource = new MatTableDataSource(this.extendedOrderLines);
    this.tableDataSource.paginator = this.paginator;
  }

  chartUpdateFlag: boolean = false;
  status:string = 'green';
  Highcharts: typeof Highcharts = Highcharts;
  chartOptions: Highcharts.Options = {
    title: {
      text: 'All the Order Lines of a product'
    },
    yAxis:{
      title : {
        text : 'Price Per Unit'
      },
    },
    tooltip: {
      formatter: function () {
          return '' + this.key +'<br/>'+
              '&euro; ' + this.y ;
      }
  },
    // xAxis: {
    //   type: 'datetime',      
    //   labels: {
    //     format: '{value:%e-%b-%Y}'
    //   }
    // },
    xAxis: {
      type: 'category',
      labels: {
        enabled: true,
        rotation: -30,
        format: '{value:%e-%b-%Y}'
      },
    },
    series: [{
      data: [1, 2, 3],
      type: 'column'
    }]
  };

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  ngOnInit(): void {
    this.updateChart();
    this.updateStatus();
  }

  ngAfterViewInit(): void {
    this.tableDataSource.paginator = this.paginator;
    this.tableDataSource.sort = this.sort;
  }

  updateChart(): void {
    this.chartSeries[0].data = [];
    this.createChartSeries();
    this.chartOptions.series = this.chartSeries;
    this.chartUpdateFlag = true;
  }

  updateStatus():void{
    let number = this.extendedOrderLines[this.extendedOrderLines.length - 1].pricePerUnit
    this.status = (number <=2) ? 'red':'green';
  }

  createChartSeries(): void {
    this.extendedOrderLines.map((val, index) => {
      let timestamp = Date.parse(val.createdDate) //Reformat the datetime to timestamp
      // let formattedDate = new Date(timestamp)
      // this.chartSeries[0].data.push([val.createdDate, val.pricePerUnit]);
      this.chartSeries[0].data.push([timestamp, val.pricePerUnit]);
    })
    console.log(this.chartSeries);
  }

  announceSortChange(sortState: Sort) {
    // This example uses English messages. If your application supports
    // multiple language, you would internationalize these strings.
    // Furthermore, you can customize the message to add additional
    // details about the values being sorted.
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }
}
