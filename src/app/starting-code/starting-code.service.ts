import { Injectable } from '@angular/core';
import { result } from 'cypress/types/lodash';
import { ExtendedOrderLine } from '../core/models/extended-order-line.model';
import { GlobalDataService } from '../core/services/api.mock.service';

@Injectable({
  providedIn: 'root'
})
export class StartingCodeService {

  constructor(
    /**
     * injecting the dependency
     */
    private gds: GlobalDataService
  ) { }

  async getOrderLineData(){
    let result = await this.gds.getExtendedOrderLines();
    return result;
  }

  async changeDateFormat(data:Array<ExtendedOrderLine>){
    let results = data.map((val, index) =>{
      
    })
  }

}
