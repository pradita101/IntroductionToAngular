export interface  ExtendedOrderLine {
    regionId: string;
    orderLineId: string;
    invoiceId: string;
    productId: string;
    customerId: string;
    createdDate: string;
    quantity: number;
    pricePerUnitVat: number;
    purchaseCostPerUnit: number;
    shippingCostPerUnit: number;
    otherCostPerUnit: number;
    pricePerUnit: number;
    totalCostPerUnit: number;
    marginPerUnit: number;
    feePercentage: number;
    vatPercentage: number;
    attributeA: string;
    attributeB: string;
    attributeC: string;

    // customer fields
    customerName: string;
    customerAttributeA: string;
    customerAttributeB: string;
    customerAttributeC: string;
}