import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageComponentSpecialistComponent } from './page-component-specialist.component';

describe('PageComponentSpecialistComponent', () => {
  let component: PageComponentSpecialistComponent;
  let fixture: ComponentFixture<PageComponentSpecialistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageComponentSpecialistComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PageComponentSpecialistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
