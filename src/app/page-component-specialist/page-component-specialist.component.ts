import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { ExtendedOrderLine } from '../core/models/extended-order-line.model';
import { GlobalDataService } from '../core/services/api.mock.service';
import { MatTableDataSource } from '@angular/material/table';


@Component({
  selector: 'app-page-component-specialist',
  templateUrl: './page-component-specialist.component.html',
  styleUrls: ['./page-component-specialist.component.css']
})
export class PageComponentSpecialistComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  
}
