import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageDataSpecialistComponent } from './page-data-specialist.component';

describe('PageDataSpecialistComponent', () => {
  let component: PageDataSpecialistComponent;
  let fixture: ComponentFixture<PageDataSpecialistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageDataSpecialistComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PageDataSpecialistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
