import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainComponent } from './main/main.component';
import { PageTesterComponent } from './page-tester/page-tester.component';
import { PageDataSpecialistComponent } from './page-data-specialist/page-data-specialist.component';
import { PageComponentSpecialistComponent } from './page-component-specialist/page-component-specialist.component';
import { MaterialModule } from 'src/material.module';
import { PageIntroductionComponent } from './page-introduction/page-introduction.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { StartingCodeComponent } from './starting-code/starting-code.component';
import { MatPaginatorModule } from "@angular/material/paginator";
import { FlexModule } from "@angular/flex-layout";
import { TruncatePipe } from './starting-code/truncate.pipe';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    PageTesterComponent,
    PageDataSpecialistComponent,
    PageComponentSpecialistComponent,
    PageIntroductionComponent,
    StartingCodeComponent,
    TruncatePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexModule,
    MaterialModule,
    MatPaginatorModule,
    HighchartsChartModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
