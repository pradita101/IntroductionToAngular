import {ChangeDetectorRef, Component, AfterViewInit, ViewChild} from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import { Links } from '../core/models/links.model';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent  implements AfterViewInit{

  mobileQuery: MediaQueryList;

  filterNav: Links[] = [
    {name : 'Introduction', link : '/'},
    {name : 'Starting Code', link : '/starting-code'},
    {name : '1. Component Specialist', link : '/component-specialist'},
    {name : '2. Data Specialist', link : '/data-specialist'},
    {name : '3. Tester', link : '/tester'},
  ];

  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() : void {

  }

}
