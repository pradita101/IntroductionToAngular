import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageComponentSpecialistComponent } from './page-component-specialist/page-component-specialist.component';
import { PageDataSpecialistComponent } from './page-data-specialist/page-data-specialist.component';
import { PageIntroductionComponent } from './page-introduction/page-introduction.component';
import { PageTesterComponent } from './page-tester/page-tester.component';
import { StartingCodeComponent } from './starting-code/starting-code.component';

const routes: Routes = [
  {path : '', component: PageIntroductionComponent},
  {path : 'starting-code', component: StartingCodeComponent},
  {path : 'component-specialist', component: PageComponentSpecialistComponent},
  {path : 'data-specialist', component : PageDataSpecialistComponent},
  {path : 'tester', component : PageTesterComponent},
 ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
